
MC Texture Viewer

# About
This simple Minecraft texture previewer that allows for real time tiling and animation previewing.

Check the builds folder for the latest version 

# Referenced libraries: 
- JSON in Java: [https://github.com/stleary/JSON-java](https://github.com/stleary/JSON-java)