package com.legacy.animation_viewer;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.Timer;

import org.json.JSONArray;
import org.json.JSONObject;

public class TexturePanel extends JPanel implements ActionListener
{
	private static final long serialVersionUID = 7724318153980966825L;
	protected final Timer timer;
	private BufferedImage fullImage;
	protected int tileWidth = 1;
	private Image[] images = new Image[0];
	private int currentFrame = 0;
	protected int[] animationCycle = new int[] { 0 };

	public TexturePanel()
	{
		timer = new Timer(50, this);
		buildImage();
		timer.start();
	}

	private void buildImage()
	{
		try
		{
			if (TextureViewer.INSTANCE != null && !TextureViewer.INSTANCE.texturePath.isEmpty())
			{
				File file = new File(TextureViewer.INSTANCE.texturePath);
				if (file.exists())
					this.fullImage = ImageIO.read(file);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		if (this.fullImage != null)
		{
			int width = this.fullImage.getTileWidth();
			int maxFrames = this.fullImage.getHeight() / width;

			if (this.animationCycle == null)
			{
				this.animationCycle = new int[maxFrames];
				for (int i = 0; i < maxFrames; i++)
				{
					this.animationCycle[i] = i;
				}
			}

			if (this.fullImage.getTileHeight() % width == 0)
			{
				this.images = new Image[maxFrames];

				for (int i = 0; i < maxFrames; i++)
				{
					BufferedImage original = this.fullImage.getSubimage(0, width * i, width, width);
					BufferedImage resized = new BufferedImage(TextureViewer.INSTANCE.displayWindowWidth, TextureViewer.INSTANCE.displayWindowWidth, BufferedImage.TYPE_INT_RGB);
					Graphics2D g = resized.createGraphics();
					g.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_SPEED);

					int imgWidth = TextureViewer.INSTANCE.displayWindowWidth / this.tileWidth;
					for (int x = 0; x < this.tileWidth; x++)
						for (int y = 0; y < this.tileWidth; y++)
							g.drawImage(original, imgWidth * x, imgWidth * y, imgWidth + (imgWidth * x), imgWidth + (imgWidth * y), 0, 0, original.getWidth(), original.getHeight(), null);

					g.dispose();

					this.images[i] = resized;
				}
			}
			else
			{
				System.out.println("WARNING: Texture size is not correct");
				this.images = new Image[] {};
			}
		}
	}

	public void buildAnimation()
	{
		File mcmetaFile = new File(TextureViewer.INSTANCE.texturePath + ".mcmeta");
		if (mcmetaFile.exists())
		{
			try
			{
				BufferedReader reader = new BufferedReader(new FileReader(mcmetaFile));
				String jsonText = "";
				String line;
				while ((line = reader.readLine()) != null)
				{
					jsonText = jsonText + line;
				}

				JSONObject mcmetaJson = new JSONObject(jsonText);
				JSONObject animation = JsonUtil.getOrDefault(mcmetaJson, "animation", JSONObject.class, null);
				if (animation != null)
				{
					JSONArray frames = JsonUtil.getOrDefault(animation, "frames", JSONArray.class, null);
					if (frames != null)
					{
						int frametime = JsonUtil.getOrDefault(animation, "frametime", Integer.class, 1);
						TextureViewer.INSTANCE.frameSpinner.setValue(frametime);

						int l = frames.length();
						this.animationCycle = new int[l];
						for (int i = 0; i < l; i++)
						{
							this.animationCycle[i] = frames.getInt(i);
						}
					}
					else
					{
						this.animationCycle = null;
					}
				}
				else
				{
					this.animationCycle = new int[] { 0 };
				}

				reader.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
		else
		{
			TextureViewer.INSTANCE.frameSpinner.setValue(1);
			this.animationCycle = new int[] { 0 };
			this.currentFrame = 0;
		}
	}

	@Override
	public void paint(Graphics g)
	{
		super.paint(g);
		g.clearRect(0, 0, TextureViewer.INSTANCE.displayWindowWidth, TextureViewer.INSTANCE.displayWindowWidth);
		if (this.animationCycle != null && this.currentFrame < this.animationCycle.length)
		{
			int frame = this.animationCycle[this.currentFrame];
			if (frame < this.images.length)
				g.drawImage(this.images[frame], 0, 0, this);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		this.repaint();
		this.currentFrame++;
		if (this.animationCycle == null || this.currentFrame >= this.animationCycle.length)
		{
			this.buildAnimation();
			this.currentFrame = 0;
			this.buildImage();
		}
	}
}