package com.legacy.animation_viewer;

import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class MouseWheelJSpinner extends JSpinner implements MouseWheelListener
{
	private static final long serialVersionUID = 4001663578916905301L;

	public MouseWheelJSpinner(SpinnerNumberModel spinnerModel)
	{
		super(spinnerModel);
		this.addMouseWheelListener(this);
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e)
	{
		SpinnerNumberModel model = (SpinnerNumberModel) this.getModel();
		int val = Math.min(Math.max((int) model.getMinimum(), (int) this.getValue() - e.getWheelRotation()), (int) model.getMaximum());
		this.setValue(val);
	}
}
