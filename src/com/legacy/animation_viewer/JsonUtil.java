package com.legacy.animation_viewer;

import org.json.JSONObject;

public class JsonUtil
{
	public static <T> T getOrDefault(JSONObject json, String key, Class<T> defaultClass, T defaultVal)
	{
		try
		{
			return defaultClass.cast(json.get(key));
		}
		catch (Exception e)
		{
			return defaultVal;
		}
	}
}
