package com.legacy.animation_viewer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.function.Function;

/**
 * This class is used to create, read, and write to a file to store settings for
 * your application.
 * <p>
 * This file is provided as is. You are free to make modifications,
 * redistribute, include in projects, etc. Do what you want.
 * 
 * @author SilverDavid
 *
 */
public class SettingsFile
{
	private static final String MAIN_SETTINGS_FOLDER = "SilverDavid";
	private final File file;
	private final Setting<?>[] settings;

	/**
	 * @param settingsFile
	 *            The file that settings will be stored in
	 * @param settings
	 *            The settings for this file
	 */
	public SettingsFile(File settingsFile, Setting<?>... settings)
	{
		this.file = settingsFile;
		this.settings = settings;
	}

	/**
	 * @param name
	 *            The path of the settings file.
	 *            <p>
	 *            Prefixed by
	 *            <code>System.getProperty("user.home") + "/SilverDavid/"</code>
	 *            <p>
	 *            You can change the "SilverDavid" part by modifying
	 *            {@link SettingsFile#MAIN_SETTINGS_FOLDER}
	 * @param settings
	 *            The settings for this file
	 */
	public SettingsFile(String name, Setting<?>... settings)
	{
		String userHome;
		try
		{
			userHome = System.getProperty("user.home");
		}
		catch (Throwable t)
		{
			t.printStackTrace();
			userHome = null;
		}
		this.file = userHome != null ? new File(userHome + "/" + MAIN_SETTINGS_FOLDER + "/" + name + ".txt") : null;
		this.settings = settings;
	}

	/**
	 * Saves the file.
	 * <p>
	 * More formally, writes the settings to <code>this.file</code> on the machine
	 * running it.
	 */
	public void save()
	{
		if (this.file != null)
		{
			try
			{
				this.file.getParentFile().mkdirs();

				PrintWriter writer = new PrintWriter(this.file);
				for (Setting<?> setting : this.settings)
					writer.println(setting.key() + "=" + setting.toString());
				writer.close();
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
		}
	}

	/**
	 * Loads the file.
	 * <p>
	 * More formally, loads the settings from <code>this.file</code> on the machine
	 * running it.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void load()
	{
		if (this.file != null)
		{
			try
			{
				if (this.file.exists())
				{
					Scanner reader = new Scanner(this.file);
					while (reader.hasNext())
					{
						String[] data = reader.nextLine().split("=", 2);
						if (data.length > 1)
							for (Setting setting : this.settings)
								if (setting.key().equals(data[0]))
									setting.set(setting.fromString(data[1]));
					}
					reader.close();
				}
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
		}
	}

	/**
	 * Prints the current data to the console for debugging purposes.
	 */
	public void printData()
	{
		System.out.println("Current settings for: " + this.file);
		for (Setting<?> setting : this.settings)
			System.out.println("  " + setting.key + " = " + setting.toString());
	}

	/**
	 * Stores a given setting with a <code>fromString</code> and
	 * <code>toString</code> function for both saving and loading.
	 * 
	 * @author SilverDavid
	 *
	 * @param <T>
	 *            The object represented by this setting
	 */
	public static class Setting<T>
	{
		private final String key;
		private final Function<String, T> fromString;
		private final Function<T, String> toString;
		private T object = null;
		private T defaultVal = null;

		public static <T> Setting<T> setting(String key, Function<String, T> fromString, Function<T, String> toString)
		{
			return new Setting<T>(key, fromString, toString);
		}

		public static <T> Setting<T> setting(String key, Function<String, T> fromString)
		{
			return new Setting<T>(key, fromString, T::toString);
		}

		public static Setting<String> stringSetting(String key)
		{
			return new Setting<String>(key, s -> s, s -> s);
		}

		public static Setting<Integer> intSetting(String key)
		{
			return new Setting<Integer>(key, Integer::parseInt, o -> o.toString());
		}

		public static Setting<Double> doubleSetting(String key)
		{
			return new Setting<Double>(key, Double::parseDouble, o -> o.toString());
		}

		private Setting(String key, Function<String, T> fromString, Function<T, String> toString)
		{
			if (key.contains("="))
				throw new IllegalArgumentException();
			this.key = key;
			this.fromString = fromString;
			this.toString = toString;
		}

		/**
		 * Sets the default value of this setting to be used in the event that the
		 * stored value is null or no settings file was present.
		 * <p>
		 * If not set, the default value for the setting will be null.
		 * 
		 * @param defaultVal
		 * @return The Setting instance
		 */
		public Setting<T> withDefault(T defaultVal)
		{
			this.defaultVal = defaultVal;
			return this;
		}

		/**
		 * Gets the string value of the object held in this setting created from the
		 * setting's <code>toString</code> function.
		 * 
		 * @return The string value of the object held in this setting, or "null" if the
		 *         object is null
		 */
		@Override
		public String toString()
		{
			T obj = this.get();
			return obj != null ? this.toString.apply(obj) : "null";
		}

		/**
		 * Gets the object associated with the passed string created from the setting's
		 * <code>fromString</code> function.
		 * 
		 * @param val
		 *            The string to parse
		 * @return The object representation of val, or the setting's default value if
		 *         parsing failed.
		 */
		public T fromString(String val)
		{
			try
			{
				return this.fromString.apply(val);
			}
			catch (Throwable t)
			{
				return this.defaultVal;
			}
		}

		/**
		 * Gets the key to save the setting under.
		 * 
		 * @return The name that this setting should be stored under.
		 */
		public String key()
		{
			return this.key;
		}

		/**
		 * Gets the value stored in this setting.
		 * 
		 * @return The object stored in this setting. Returns the default value if the
		 *         object is null.
		 */
		public T get()
		{
			return this.object != null ? this.object : this.defaultVal;
		}

		/**
		 * Sets the value stored in this setting.
		 * 
		 * @param newValue
		 */
		public void set(T newValue)
		{
			this.object = newValue;
		}
	}
}
