package com.legacy.animation_viewer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.legacy.animation_viewer.SettingsFile.Setting;

public class TextureViewer implements ActionListener, ChangeListener
{
	protected static final TextureViewer INSTANCE = new TextureViewer();

	private Setting<String> prevDirectory = Setting.stringSetting("previous_directory");
	private SettingsFile settingsFile = new SettingsFile("TextureViewer/settings", this.prevDirectory);

	protected TexturePanel texturePanel;
	protected JFrame frame;
	private JButton browseButton;
	protected MouseWheelJSpinner frameSpinner;
	private MouseWheelJSpinner tileWidthSpinner;
	protected final int displayWindowWidth;
	protected String texturePath;

	public static void main(String[] args)
	{

	}

	public TextureViewer()
	{
		this.settingsFile.load();
		this.settingsFile.printData();

		displayWindowWidth = 256;
		texturePath = "";

		frame = new JFrame();

		frame.setBounds(10, 10, displayWindowWidth + 6, displayWindowWidth + 100);
		frame.setTitle("Texture Viewer");
		frame.setAlwaysOnTop(true);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Texture display panel
		texturePanel = new TexturePanel();
		texturePanel.setLayout(null);
		frame.add(texturePanel);

		// Framerate spinner
		frameSpinner = new MouseWheelJSpinner(new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1));
		frameSpinner.setBounds(185, 290, 50, 20);
		frameSpinner.addChangeListener(this);
		texturePanel.add(frameSpinner);

		JLabel frameRateLabel = new JLabel("Framerate");
		frameRateLabel.setBounds(180, 270, 70, 20);
		texturePanel.add(frameRateLabel);

		// Texture button
		browseButton = new JButton("Texture");
		browseButton.setBounds(83, 290, 80, 20);
		browseButton.addActionListener(this);
		texturePanel.add(browseButton);

		// Tiling spinner
		tileWidthSpinner = new MouseWheelJSpinner(new SpinnerNumberModel(1, 1, 64, 1));
		tileWidthSpinner.setBounds(10, 290, 50, 20);
		tileWidthSpinner.addChangeListener(this);
		texturePanel.add(tileWidthSpinner);

		JLabel tileWidthLable = new JLabel("Tile Width");
		tileWidthLable.setBounds(8, 270, 70, 20);
		texturePanel.add(tileWidthLable);

		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent event)
	{
		if (event.getSource() == browseButton)
		{
			JFileChooser chooser = new JFileChooser();
			try
			{
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			}
			catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e)
			{
				e.printStackTrace();
			}

			if (this.prevDirectory.get() != null)
			{
				File dir = new File(this.prevDirectory.get());
				if (dir.exists())
					chooser = new JFileChooser(dir);
			}
			chooser.setDialogTitle("Select Texture File");
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			chooser.setFileFilter(new FileNameExtensionFilter(null, "png"));
			chooser.setAcceptAllFileFilterUsed(false);

			if (chooser.showSaveDialog(this.texturePanel) == JFileChooser.APPROVE_OPTION)
			{
				this.prevDirectory.set(chooser.getCurrentDirectory().getPath());
				this.texturePath = chooser.getSelectedFile().getPath();
				this.texturePanel.buildAnimation();
			}

			this.settingsFile.save();
		}
	}

	@Override
	public void stateChanged(ChangeEvent event)
	{
		if (event.getSource() == frameSpinner)
		{
			int delay = (int) frameSpinner.getValue();
			this.texturePanel.timer.setDelay(delay * 50);
		}
		else if (event.getSource() == tileWidthSpinner)
		{
			this.texturePanel.tileWidth = (int) tileWidthSpinner.getValue();
		}
	}
}
